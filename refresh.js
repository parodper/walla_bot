// This code is supposed to run from the wallapop.com/app/catalog/list page
// Change sleep times as needed

function sleep(s) {
	return new Promise(resolve => setTimeout(resolve, s * 1000));
}

async function main() {
	products = document.getElementsByClassName("btn-edit");
	page_load_wait = 5;

	//Go through all the products in the list
	i = 0;
	while (true) {
		if(i == productos.length) {
			i = 0;
		}

		//Edit the product
		productos[i].click();
		await sleep(page_load_wait);

		raiz = document.getElementById("prueba");
		
		//Check to see if the page loads too slowly.
		if(raiz === null) {
			console.log("Page load too slow");
			page_load_wait++;
		}

		//Exit from the edit page
		raiz.getElementsByClassName("btn-block")[0].click();
		await sleep(10); //Wait for the list to load
		
		i++;
	}
}

main();

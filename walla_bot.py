# -*- coding: utf-8 -*-
#	YOU HAVE TO PUBLISH AT LEAST ONE PRODUCT IN THE TARGET CITY YOU WANT THE
#	REST OF THEM. THEN IT WILL REMEMBER AND MARK THAT CITY ALL THE OTHER
#	UPLOADS. IT'S A WORKAROUND

### IMPORTS ###

## Local
import cookies as Cookies
import product as Products
import walla_bot_funcs as Aux

## Builtin
import logging
import json

## Selenium
import selenium

### CONSTANTS ###

DRIVER_PATH = "selenium_drivers/geckodriver"
PRODUCTS_FILE = "prods.json"
logger = logging.getLogger('output')

### FUNCTIONS ###

def set_driver():
	logging.info(f'Setting driver to {DRIVER_PATH}')
	try:
		return selenium.webdriver.Firefox()
	except:
		logging.critical('Driver could not be set')

def upload(d):
	logger.info('Start uploading products')
	
	# Iterate products
	
	with open(PRODUCTS_FILE, "r") as prods_file:
		products = json.load(prods_file)
	
	logger.info(f'Loaded {len(products)} products from {PRODUCTS_FILE}')
	
	n_prods = 1 # Used to random sleep every x products
	for prod in products:
		# Upload chunks of 3 and rest
		if n_prods % 3 == 0:
			Aux.random_sleep(20,40)
		
		#Parsing products file
		title           = prod['title']
		desc            = prod['ad_text']
		state           = prod['state']			
		price           = prod['price']
		pic_paths       = prod['pics']
		
		category        = prod['category']
		try:
			subcategory = prod['subcategory']
		except KeyError:
			subcategory = ''
		try:
			specify     = prod['specify']
		except KeyError:
			specify     = ''
		try:
			brand       = prod['brand']
		except KeyError:
			brand       = ''
		try:
			model       = prod['model']
		except KeyError:
			model       = ''
		try:
			gender      = prod['gender']
		except KeyError:
			gender      = ''
		try:
			size        = prod['size']
		except KeyError:
			size        = ''
		try:
			location    = prod['location']
		except KeyError:
			location    = ''
		try:
			weight      = prod['weight']
		except KeyError:
			weight      = None
		
		logger.debug(f'Uploading {title}({price}€) @ {location}, {state}, \
			{category}/{subcategory}/{specify}/{brand}/{gender}/{size}: {desc}')
		
		if Products.fill_ad( d, prod_title = title, state = state,
			description = desc, price = price, location = location,
			pics = pic_paths, category = category, subcategory = subcategory,
			specify = specify, brand = brand, model = model,
			gender = gender, size = size, weight = weight ):
			logger.info(f'Uploaded {title}')
		n_prods = n_prods + 1
		Aux.random_sleep(1,3)
	logger.info(f'Published all ads: {n_prods - 1}')

if __name__ == "__main__":
	logger.setLevel(logging.DEBUG)
	sh = logging.StreamHandler()
	sh.setLevel(logging.DEBUG)
	sh.setFormatter(logging.Formatter('%(funcName)s: %(message)s'))
	logger.addHandler(sh)
	
	driver = set_driver()
	
	# Try to log in
	logger.info('Loging in')
	Cookies.login_cookies(driver)
	
	upload(driver)
	logger.info('All jobs done. Exiting...')
	driver.close()

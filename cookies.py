#
#	FUNCTIONS FOR LOGIN IN AND MANAGING COOKIES
#

### IMPORTS ###

## Local
import walla_bot_funcs as Aux

## Builtin
import logging
import pickle

## Selenium
import selenium
from selenium.common.exceptions import TimeoutException

### CONSTANTS ###

COOKIES_FILE = "cookies"
logger = logging.getLogger('output')

### FUNCTIONS ###

# This logins the user and stores the cookies in the chosen file
def login_cookies(d, cookies_file = COOKIES_FILE):
	url = 'https://es.wallapop.com'
	
	try:
		logger.info(f'Storing login cookies in {cookies_file}')
		d.get(url)
		try:
			cookies = pickle.load(open(cookies_file, "rb"))
			logger.info('Loading cookies, please wait...')
			for cookie in cookies:
				d.add_cookie(cookie)
			d.get(url)
		except FileNotFoundError:
			logger.info('Cookies not found, please log in and press enter')
			# Save cookies
			pickle.dump( d.get_cookies(), open(cookies_file, "wb") )
			logger.info('Loged in successfully')
		input("Press enter to continue...")
		return True
	except Exception as e:
		logger.error(f'Error {e}')

	return False

#
#	MISC SHARED FUNCTIONS
#

### IMPORTS ###

## Builtin
from time import sleep
import logging
import random

## Selenium
import selenium
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.firefox.webdriver    import WebDriver
from selenium.webdriver.common.by            import By
from selenium.webdriver.common.keys          import Keys
from selenium.common.exceptions              import TimeoutException

### CONSTANTS ###

COOKIES_FILE = "cookies"
logger = logging.getLogger('output')

### FUNCTIONS ###

# Click on the XPath
def my_click(d, xpath, scroll = False):
	try:
		element = d.find_element(By.XPATH, xpath)
		#Action chains has little accuracy, lots of mistakes clicking in the
		#	wrong place
		if scroll:
			d.execute_script("arguments[0].scrollIntoView();", element)
		actions = ActionChains(d)
		actions.move_to_element(element).click().perform()
	except Exception as e:
		logger.warn(e)

# Types text into the XPath box
def type_text(d, text, xpath, end= None):
	''' d, xpath, text, end: tab, enter'''
	element = d.find_element(By.XPATH, xpath)
	element.send_keys(text)
	
	if(end != None):
		element.send_keys(end)
	#element.send_keys(Keys.TAB)
	#element.send_keys(Keys.RETURN)

def type_slow(d,text,xpath, end=None):
	# Given driver, text and xpath input it types the text in the xpath input
	
	try: # It gets a weird exception, probably some python bug
		element = d.find_element_by_xpath(xpath)
		for character in text:
			element.send_keys(character)
			sleep(random.uniform(0.05,0.08))

		if(end != None):
			element.send_keys(end)
	
	except selenium.common.exceptions.ElementNotInteractableException:
		logger.warn('Not interactable exception, passing')
		pass

def check_presence(driver, xpath):
	logger.debug(f'Checking if XPath is present: {xpath}')
	try:
		element = driver.find_element(By.XPATH, xpath)

		if element != None:
			logger.debug(f'XPath present: {xpath}')
			return True
	except:
		logger.debug(f'XPath NOT present: {xpath}')
		return False

def random_sleep(a, b):
	from random import randint
	sleep(randint(a, b))

def press_enter(d):
	actions = ActionChains(d)
	actions.send_keys(Keys.ENTER)
	actions.perform()

#
#	FUNCTIONS FOR FILLING THE FIELDS OF THE AD
#

''' Ad fields:
	Title ->             title: Text,
	Description ->       ad_text: Text,
	Price ->             price: Number
	Pictures ->          pics: [ (strings with filepaths) ]
	Category ->          category: Full category name,
	State ->             state: See select_prod_state() for more info
	Subcategory ->       subcategory: Full subcategory (optional)
	¿Need to specify? -> specify: Full specify text (optional)
	Brand ->             brand: Brand canonical name (optional)
	Model ->             model: Model canonical name (optional)
	Gender ->            gender: Either "Hombre" or "Mujer" (optional)
	Size ->              size: Full size name (optional)
	Location ->          location: Not implemented
	Weight ->            weight: Number (optional)
'''

### IMPORTS ###

## Local
import walla_bot_funcs as Aux

## Builtin
import logging
import os

## Selenium
import selenium
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.webdriver     import WebDriver
from selenium.webdriver.common.by            import By
from selenium.webdriver.common.keys          import Keys
from selenium.webdriver.support.wait         import WebDriverWait

### CONSTANTS ###

logger = logging.getLogger('output')

### FUNCTIONS ###

# Inserts the pictures contained in pics (vector of strings with filenames)
def insert_pics(d, pics):
	logger.debug(f'Inserting images: {pics}')
	pic_inputs = d.find_elements(By.XPATH, '//input[@type="file"]')

	# wallapop has 10 different pic input paths, one for each pic, so zip each
	#	pic with its unique input
	zipped_list = zip(pics, pic_inputs)
	
	for entry in zipped_list:
		try:
			pic = entry[0]
			input = entry[1]
			
			input.send_keys(os.path.abspath(pic))
			logger.debug(f'Inserted pic {pic}')
		except Exception as e :
			logger.error(f'Could not insert picture {pic}')

def select_prod_state(d, prod_state):
	state = {
		'unboxed': 'Sin estrenar',
		'new': 'Nuevo',
		'not_new': 'En buen estado',
		'aceptable': 'En condiciones aceptables',
		'gave_all': 'Lo ha dado todo'
	}[prod_state]
	logger.debug(f'State {state}')
	
	try:
		Aux.my_click(d, '//div[contains(text(), "Escoge un estado")]')
		#Aux.my_click(d, f'//li[contains(text(), "{state}")]')
		Aux.my_click(d, '//div[@class="filter"]/input')
		Aux.type_slow(d, state, '//div[@class="filter"]/input', end=Keys.RETURN)
		
	except Exception as e:
		logger.warn(f'Couldn\'t select State')

def select_categories(d, category, subcategory = '', specify = '' ):
	# Checks if optional fields are active
	def field_active(d, id_name):
		try:
			d.find_element(By.ID, id_name).find_element(by=By.CLASS_NAME, value='disabled')
			logger.debug(f'{id_name} is disabled')
			return False
		except selenium.common.exceptions.NoSuchElementException as e:
			logger.debug(f'{id_name} is enabled')
			return True
	
	#if category == 'Móviles y Telefonía':
	#	category = ' Móviles y Telefonía ' # Notice it has spaces at both sides
	logger.debug(f'Selected category "{category}"')
	
	Aux.my_click(d, '//tsl-dropdown[@placeholder="Categoría"]')
	#Aux.my_click(d, f'//div[contains(text(), "{category}")]') 
	Aux.my_click(d, '//div[@class="filter"]/input')
	Aux.type_slow(d, category, '//div[@class="filter"]/input', end=Keys.RETURN)
	Aux.random_sleep(2,4)
	
	#Check if there are subcategories
	if subcategory != '':
		if field_active(d, 'objectType'):
			logger.debug(f'Selected subcategory {subcategory}')
			Aux.my_click(d, '//tsl-dropdown[@placeholder="Subcategoría"]')
			while True:
				selection = d.find_element(By.XPATH, value='//li[@class="highlighted"]')
				logger.debug(f'Selected {selection.text}')
				if selection.text == subcategory:
					ActionChains(d).send_keys(Keys.RETURN).perform()
					break
				else:
					ActionChains(d).send_keys(Keys.ARROW_DOWN).perform()
				Aux.random_sleep(1, 1)
			
			#Aux.my_click(d, f'//span[contains(text(), "{subcategory}")]')
			#Check if we need to specify more
			Aux.random_sleep(4,4)
			if specify != '':
				if field_active(d, 'objectType2'):
					logger.debug(f'Specified {specify}')
					Aux.my_click(d, '//tsl-dropdown[@placeholder="¿Podrías especificar?"]')
					Aux.my_click(d, '//tsl-dropdown[@placeholder="Subcategoría"]')
					while True:
						selection = d.find_element(By.XPATH, value='//li[@class="highlighted"]')
						logger.debug(f'Selected {selection.text}')
						if selection.text == specify:
							ActionChains(d).send_keys(Keys.RETURN).perform()
							break
						else:
							ActionChains(d).send_keys(Keys.ARROW_DOWN).perform()
						Aux.random_sleep(1, 1)

# Convert city to canonical location
def create_location(account_city):
	account_city = account_city.lower()
	if account_city == 'madrid':
		return 'Madrid, Madrid'
	elif account_city == 'la coruña':
		return 'La coruña, la coruña'
	else:
		return account_city + ', ' + account_city

# Upload the ad
def fill_ad(
		d,
		prod_title,
		state,
		description,
		price,
		location,
		pics,
		category,
		subcategory = '',
		specify = '',
		brand = '',
		model = '',
		gender = '',
		size = '',
		weight = None
	):
	
	d.get('https://es.wallapop.com/app/catalog/upload')
	#Wait until page loaded
	#WebDriverWait(d, 100).until(lambda x: x.find_element(By.XPATH, '//span[contains(text(), "Algo que ya no necesito")]')) 
	Aux.random_sleep(5, 7)
	logger.info(f'Uploading {prod_title}')
	
	try:
		# Click in "Algo que ya no necesito"
		Aux.my_click(d, '//span[contains(text(), "Algo que ya no necesito")]')
		Aux.random_sleep(1,3)
		
		# Type title
		Aux.type_text(d, prod_title, '//input[@placeholder="En pocas palabras..."]')
		Aux.random_sleep(1,3)
		
		# Price
		Aux.type_text(d, price, '//input[@placeholder="(No te pases)"]')
		Aux.random_sleep(1,3)
		
		# Fill description
		Aux.type_slow(d, description, '//textarea')
		Aux.random_sleep(1,3)
		
		#  Category.
		# Categories have different subcategories, and other misc fields. Not
		# even going to try to look everything up. I'll let the user choose the
		# correct subcategories
		select_categories(d, category, subcategory, specify)
		
		# State
		select_prod_state(d, state)
		Aux.random_sleep(1,3)
		
		# Phones: Fill brand and model
		if brand != '' and model != '':
			#Brand
			Aux.type_slow(d, brand, '//input[@placeholder="P. ej: Apple"]', end=Keys.TAB)
			#Model
			Aux.type_slow(d, model, '//input[@placeholder="P. ej: iPhone"]', end=Keys.TAB)
		# Clothes: Fill gender and size
		if brand != '' and gender != '':
			#Brand
			Aux.type_slow(d, model, '//input[@placeholder="P. ej: Zara"]', end=Keys.TAB)
			#Gender
			Aux.my_click(d, '//tsl-dropdown[@placeholder="Selecciona un género"]')
			Aux.my_click(d, f'//span[contains(text(), "{gender}")]')
			#Size
			if field_active(d, 'fashionSizes'):
					Aux.my_click(d, '//tsl-dropdown[@placeholder="Selecciona una talla"]')
					Aux.my_click(d, f'//span[contains(text(), "{size}")]')
		Aux.random_sleep(1,3)
		
		# Upload pics, list of absolute paths
		insert_pics(d, pics)
		Aux.random_sleep(1,3)
		
		# Click in shipping
		if weight == None:
			Aux.my_click(d, '//tsl-checkbox-form[@formcontrolname="supports_shipping"]', scroll = True)
		else:
			Aux.my_click(d, f'//span[contains(text(), "{weight}")]', scroll = True)
		Aux.random_sleep(1,3)
		
		# Location
		# TODO: Fix this. Use default, for now
		#sleep(5)
		#location_input = d.find_element_by_xpath('//input[@placeholder="Marca la localización"]')
		#location_input.send_keys(Keys.CONTROL,'a', Keys.DELETE)
		#location_input.send_keys(walla_location)
		#location_input.send_keys(Keys.RETURN)
		# iframe = d.find_element_by_xpath('//iframe[@id="tdz_ifrm"]')
		# d.switch_to.frame(iframe)
		
		# Aux.type_slow(d, walla_location, '//input[@placeholder="Marca la localización"]')
		#sleep(8)
		# my_click(d, '//button[contains(text(), "Aplicar")]')
		# apply_location_button = d.find_element_by_xpath('//button[contains(text(), "Aplicar")]')
		#apply_location_button = d.find_element(By.XPATH, '//button[contains(text(), "Aplicar")]')
		#apply_location_button.click()
		# apply_location_button.click()
		# actions = ActionChains(d)
		# actions.move_to_element(apply_location_button).click().perform()
		#print('done'
		# d.switch_to.parent_frame()
		
		#Click in submit
		Aux.my_click(d, '//button[@class="btn btn-block btn-primary"]', scroll = True)
		Aux.random_sleep(1,3)
		
		#click in close "Genial tu producto ya está subido, queres destacarlo ?"
		Aux.my_click(d , '//div[@class="modal-close light"]')
		Aux.random_sleep(1,3)
		
		# '//tsl-svg-icon[@src="/assets/icons/cross.svg"]'
		Aux.random_sleep(5,10)
		#try:
		#	d.switch_to.active_element #switch to uploaded panel
		#	if Aux.check_presence(d, '//h1[contains(text(), "¡Genial! Tu producto ya está en Wallapop")]'):
		#		return True
		#except Exception as e:
		#	logger.error(f'Could not upload {prod_title}')
		#	print(e)
		#	return False
		return True
	
	except Exception as e:
		logger.error(f'Ad upload failed: {e}')
		return False
